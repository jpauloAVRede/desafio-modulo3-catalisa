package br.com.zup.Application;

import br.com.zup.Entity.*;

public class Sistema {

    //cadastrando Imovel na lista de imoveis da calsse CatalogoImoveis
    public static void cadastrarImovel() {
        String opcao = "0";

        IO.texto("------Cadastro do Imóvel-------");

        IO.texto("Informe Código: ");
        String codigo = IO.ler().next();
        while (CatalogoImovel.verificaImovelExistente(codigo)) {
            IO.texto("Informe outro código: ");
            codigo = IO.ler().next();
        }

        IO.texto("Informe Valor Aluguel: ");
        double alugel = IO.ler().nextDouble();

        IO.texto("*****Cadastro do Endereço******");
        Endereco endereco = cadastrarEndereco();

        IO.texto("*****Cadastro do Funcionário Responsável******");
        Funcionario funcionario = cadastrarFuncionario();

        Imovel imovel = new Imovel(codigo, alugel, funcionario, endereco);

        IO.texto("***** Cadastro de Moradores ******");

        IO.texto("| Quantos moradores deseja cadastrar |");
        int qtdMorador = IO.ler().nextInt();

        for (int i = 1; i <= qtdMorador; i++) {

            boolean verifica;
            do {
                try {
                    Morador morador = cadastrarMorador(i);
                    //não deixa cadastrar mesmo cpf em um imóvel
                    imovel.verificaCpfExistente(morador.getCpf());
                    imovel.adicionarMorador(morador);
                    verifica = true;
                } catch (Exception e) {
                    IO.texto("-----   Email existe  -----");
                    IO.texto("| Por favor, insira os dados movamente |");
                    verifica = false;
                }

            }while (verifica == false);

        }

        CatalogoImovel.adicionarImovel(imovel);
    }

    public static Funcionario cadastrarFuncionario() {

        IO.texto("Informe nome: ");
        String nome = IO.ler().nextLine();

        IO.texto("CPF: ");
        String cpf = IO.ler().next();

        IO.texto("Telefone: ");
        String telefone = IO.ler().next();

        IO.texto("Cargo: ");
        String cargo = IO.ler().next();

        IO.texto("Salario: ");
        double salario = IO.ler().nextDouble();

        return new Funcionario(nome, cpf, telefone, cargo, salario);

    }

    public static Endereco cadastrarEndereco() {

        IO.texto("Nome Rua: ");
        String rua = IO.ler().nextLine();

        IO.texto("Número: ");
        String numero = IO.ler().next();

        IO.texto("CEP: ");
        String cep = IO.ler().nextLine();

        return new Endereco(rua, numero, cep);
    }

    public static Morador cadastrarMorador(int i) {
        IO.texto("----- Dados do " +i+ "° morador --------");

        IO.texto("Informe nome: ");
        String nome = IO.ler().nextLine();

        IO.texto("CPF: ");
        String cpf = IO.ler().next();

        IO.texto("Telefone: ");
        String telefone = IO.ler().next();

        IO.texto("Numero Contrato: ");
        String numeroContrato = IO.ler().next();

        Morador morador = new Morador(nome, cpf, telefone, numeroContrato);

        return morador;
    }

    public static void excluirMorador() {
        IO.texto("------ Excluir Morador do Imovel -------");

        IO.texto("Informe o CPF: ");
        String cpfExcluirMorador =  IO.ler().next();

        IO.texto("Informe o CEP do endereço: ");
        String cepImovel = IO.ler().next();

        IO.texto("Informe o numero do imóvel: ");
        String numeroImovel = IO.ler().next();

        CatalogoImovel.excluirMorador(cepImovel, numeroImovel, cpfExcluirMorador);

    }

    public static void executarSistema() {

        int opcao = 0;

        do {

            Menu.menuPrincipal();
            IO.texto("Escolha uma opção: ");
            opcao = IO.ler().nextInt();

            switch (opcao) {

                case 1:
                    cadastrarImovel();
                    break;

                case 2:
                    CatalogoImovel.exibirCatalogoImoveis();
                    break;

                case 3:
                    excluirMorador();
                    break;

                case 99:
                    IO.texto("---- Programa Finalizado -----");
                    break;

                default:
                    IO.texto("-- Opção Inválida --");
                    break;

            }

        }while (opcao != 99);

    }

}
