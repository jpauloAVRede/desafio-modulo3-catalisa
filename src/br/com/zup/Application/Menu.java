package br.com.zup.Application;

public class Menu {

    public static void menuPrincipal() {
        System.out.println("+---------------------------+");
        System.out.println("|        Menu Principal     |");
        System.out.println("+---------------------------+");
        System.out.println("|  1 - Add Imóvel Catálogo  |");
        System.out.println("|  2 - Exibir Catálogo      |");
        System.out.println("|  3 - Excluir Morador      |");
        //System.out.println("|  4 - Excluir Ingr. Prato  |");
        System.out.println("| 99 - Sair                 |");
        System.out.println("+---------------------------+");
    }

}
