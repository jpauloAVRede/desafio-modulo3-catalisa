package br.com.zup.Entity;

public class Morador extends Pessoa{
    private String numeroContrato;

    public Morador(){}

    public Morador(String nome, String cpf, String telefone, String numeroContrato) {
        super.setNome(nome);
        super.setCpf(cpf);
        super.setTelefone(telefone);
        this.numeroContrato = numeroContrato;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }


    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();

        //strBuilder.append("\nContrato: " +this.numeroContrato);
        strBuilder.append(super.toString());
        strBuilder.append("\n----------------------------------");

        return strBuilder.toString();
    }

}
