package br.com.zup.Entity;

import br.com.zup.Application.IO;

import java.util.ArrayList;
import java.util.List;

public class Imovel implements ContratoImovel{
    private String codigo;
    private double valorAluguel;
    private Funcionario funcionario;
    List<Morador> moradores = new ArrayList<>();
    private Endereco endereco;

    public Imovel() {}

    public Imovel(String codigo, double valorAluguel, Funcionario funcionario, Endereco endereco) {
        this.codigo = codigo;
        this.valorAluguel = valorAluguel;
        this.funcionario = funcionario;
        this.endereco = endereco;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    @Override
    public void verificaCpfExistente(String cpf) throws Exception {
        for (Morador elemLista : moradores) {
            if (elemLista.getCpf().equals(cpf)) {
                throw new Exception("CPF existente. Não é possível cadastrar!!!");
            }
        }
    }

    @Override
    public void adicionarMorador(Morador morador) {
        moradores.add(morador);
    }

    public void excluirMorador(String cpf) {
        int verifica = 0;
        Morador morador = null;
        for (Morador elemLista : moradores) {
            if (elemLista.getCpf().equals(cpf)) {
                morador = elemLista;
                verifica = 1;
            }
        }
        if (verifica == 1) {
            moradores.remove(morador);
            IO.texto("+------------------------------+");
            IO.texto("| Morador excluído com sucesso |");
            IO.texto("+------------------------------+");
        } else {
            IO.texto("+------------------------+");
            IO.texto("| Morador não encontrado |");
            IO.texto("+------------------------+");
        }

    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append("Código: " +this.codigo);
        strBuilder.append("\nValor Aluguel: R$ " +this.valorAluguel);
        strBuilder.append("\n----Func. Responsável---\n" +this.funcionario);
        strBuilder.append("\n----Endereço----" +this.endereco);
        strBuilder.append("\n----Moradores----" +this.moradores);

        return strBuilder.toString();

    }
}
