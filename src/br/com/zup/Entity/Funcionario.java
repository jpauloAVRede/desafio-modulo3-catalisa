package br.com.zup.Entity;

public class Funcionario extends Pessoa{
    private String cargo;
    private double salario;

    public Funcionario() { }

    public Funcionario(String nome, String cpf, String telefone, String cargo, double salario) {
        super.setNome(nome);
        super.setCpf(cpf);
        super.setTelefone(telefone);
        this.cargo = cargo;
        this.salario = salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }


    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append(super.getNome());
        strBuilder.append("\nCPF: " +super.getCpf());
        //strBuilder.append("\nCargo: " +this.cargo);
        //strBuilder.append("\nSalario: R$ " +this.salario);
        strBuilder.append("\n-------------------------------");

        return strBuilder.toString();
    }

}
