package br.com.zup.Entity;

public interface ContratoImovel {

    public void adicionarMorador(Morador morador);

    public void verificaCpfExistente(String cpf) throws Exception;

    public void excluirMorador(String cpf);

}
