package br.com.zup.Entity;

public class Endereco {
    private String nomeRua;
    private String numero;
    private String cep;

    public Endereco() {}

    public Endereco(String nomeRua, String numero, String cep) {
        this.nomeRua = nomeRua;
        this.numero = numero;
        this.cep = cep;
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append("\nRua: " +this.nomeRua);
        strBuilder.append("\nNúmero: " +this.numero);
        strBuilder.append("\nCEP: " +this.cep);
        strBuilder.append("\n---------------------------");

        return strBuilder.toString();
    }

}
