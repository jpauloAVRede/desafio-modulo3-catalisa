package br.com.zup.Entity;

public abstract class Pessoa {

    private String nome;
    private String cpf;
    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append("\nNome: " +this.nome);
        strBuilder.append("\nCPF: " +this.cpf);
        strBuilder.append("\nTelefone: " +this.telefone);

        return strBuilder.toString();
    }

}
