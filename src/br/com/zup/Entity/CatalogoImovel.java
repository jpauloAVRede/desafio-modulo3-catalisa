package br.com.zup.Entity;

import br.com.zup.Application.IO;

import java.util.ArrayList;
import java.util.List;

public class CatalogoImovel {

    private static List<Imovel> listImoveis = new ArrayList<>();

    public static void adicionarImovel(Imovel imovel) {
        listImoveis.add(imovel);
    }

    public static void exibirCatalogoImoveis() {
        IO.texto("xxxxxxx Imoveis Cadastrados xxxxxxxxx");
        for (Imovel elemLista : listImoveis) {
            System.out.println(elemLista);
            IO.texto("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        }

    }

    public static void excluirMorador(String cep, String numero, String cpf) {
        int verifica = 0;
        for (Imovel imovel : listImoveis) {
            if(imovel.getEndereco().getCep().equals(cep) && (imovel.getEndereco().getNumero().equals(numero))) {
                imovel.excluirMorador(cpf);
                verifica = 1;
            }
        }
        if (verifica == 0) {
            IO.texto(" +---------------------------------+");
            IO.texto(" | Endereço não consta no catálogo |");
            IO.texto(" +---------------------------------+");
        }
    }

    //testando novas funcionalidades
    //verificando se o imovel já existe;
    public static boolean verificaImovelExistente(String codigo) {
        for (Imovel elemLista : listImoveis) {
            if (elemLista.getCodigo().equals(codigo)) {
                IO.texto("+------------------+");
                IO.texto("| Imovel existente |");
                IO.texto("+------------------+");
                return true;
            }
        }
        return false;
    }


}
